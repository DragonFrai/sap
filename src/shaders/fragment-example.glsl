#version 140

uniform float time;
uniform vec2 resolution;
uniform vec2 mouse;

out vec4 f_color;

void main() {
    vec2 coords = gl_FragCoord.xy / resolution;
    vec2 mouse = vec2(mouse.x, -mouse.y);

    float r = 20.0;
    vec2 deltap = mouse / r - gl_FragCoord.xy / r;
    float distance = length(mouse / r - gl_FragCoord.xy / r);
    vec3 color = vec3(1.0, 0.5, 0.3);

    f_color = vec4(deltap, 0.0, 1.0);
}