use crate::X11_BACKEND;
use glium::backend::glutin::glutin::platform::unix::RawContextExt;
use glium::backend::glutin::glutin::{
    ContextBuilder, ContextCurrentState, ContextError, PossiblyCurrent, RawContext,
};
use glium::backend::Context;
use glium::backend::{Backend, Facade};
use glium::glutin;
use glium::SwapBuffersError;
use std::borrow::Borrow;
use std::cell::{Ref, RefCell};
use std::ops::Deref;
use std::os::raw::c_void;
use std::rc::Rc;
use std::sync::Arc;
use takeable_option::Takeable;
use winit::platform::unix::x11::XConnection;
use x11_dl::xlib::{Display as XDisplay, Window, XWindowAttributes};

type WContext = RawContext<PossiblyCurrent>;

#[derive(Clone)]
pub struct SapBackend {
    xconn: Arc<XConnection>,
    context: Rc<RefCell<Option<WContext>>>,
    window_attr: XWindowAttributes,
    window_id: Window,
}

impl SapBackend {
    pub fn with_display<T: ContextCurrentState>(
        xconn: &Arc<XConnection>,
        cb: ContextBuilder<T>,
        display: *mut XDisplay,
    ) -> Self {
        let xconn = Arc::clone(xconn);
        let xlib = &xconn.xlib;

        unsafe {
            let screen_id = (xlib.XDefaultScreen)(display);
            let window_id = (xlib.XRootWindow)(display, screen_id);

            let mut attrs = std::mem::zeroed();
            (xlib.XGetWindowAttributes)(display, window_id, &mut attrs);

            let ctx = cb
                .build_raw_x11_context(Arc::clone(&xconn), window_id)
                .unwrap();

            let ctx = ctx.treat_as_current();

            Self {
                xconn: xconn,
                context: Rc::new(RefCell::new(Some(ctx))),
                window_attr: attrs,
                window_id: window_id,
            }
        }
    }

    pub fn new<T: ContextCurrentState>(xconn: &Arc<XConnection>, cb: ContextBuilder<T>) -> Self {
        Self::with_display(xconn, cb, xconn.display)
    }
}

impl SapBackend {
    pub fn mouse_position(&self) -> (u32, u32) {
        let window = self.window_id;
        let display = self.xconn.display;

        let mut root_return = 0;
        let mut _child_return = 0;
        let mut root_x_return = 0;
        let mut root_y_return = 0;
        let mut _child_x_return = 0;
        let mut _child_y_return = 0;
        let mut _mask_return = 0;

        unsafe {
            (self.xconn.xlib.XQueryPointer)(
                display,
                window,
                &mut root_return,
                &mut _child_return,
                &mut root_x_return,
                &mut root_y_return,
                &mut _child_x_return,
                &mut _child_y_return,
                &mut _mask_return,
            );
        }

        (root_x_return as _, root_y_return as _)
    }

    fn context_wrapper(&self) -> Ref<'_, Option<WContext>> {
        self.context.deref().borrow()
    }
}

impl Facade for SapBackend {
    fn get_context(&self) -> &Rc<Context> {
        unimplemented!()
    }
}

unsafe impl Backend for SapBackend {
    fn swap_buffers(&self) -> Result<(), SwapBuffersError> {
        self.context_wrapper()
            .as_ref()
            .unwrap()
            .swap_buffers()
            .map_err(|e| match e {
                ContextError::ContextLost => SwapBuffersError::ContextLost,
                ContextError::OsError(e) => panic!("OS Error while swapping buffers: {:?}.", e),
                ContextError::IoError(e) => panic!("I/O Error while swapping buffers: {:?}.", e),
                ContextError::FunctionUnavailable => {
                    panic!("function unavailable error while swapping buffers.")
                }
            })
    }

    unsafe fn get_proc_address(&self, symbol: &str) -> *const c_void {
        self.context_wrapper()
            .as_ref()
            .unwrap()
            .get_proc_address(symbol)
    }

    fn get_framebuffer_dimensions(&self) -> (u32, u32) {
        let attr = self.window_attr;
        (attr.width as _, attr.height as _)
    }

    fn is_current(&self) -> bool {
        let ctx = self.context_wrapper();
        let ctx = ctx.as_ref();
        ctx.map_or_else(|| unreachable!("Context is None."), |ctx| ctx.is_current())
    }

    unsafe fn make_current(&self) {
        let ctx = self.context.borrow_mut().take().unwrap();
        let ctx = ctx.make_current().unwrap();
        self.context.borrow_mut().replace(ctx);
    }
}
