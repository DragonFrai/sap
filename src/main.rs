mod draw;
mod sap_backend;
mod state;

#[macro_use]
extern crate glium;
#[macro_use]
extern crate lazy_static;

use lazy_static::*;

use glium::index::PrimitiveType;
#[allow(unused_imports)]
use glium::{glutin, Surface};
use std::ffi::{CStr, CString};
use x11_dl::xlib::{
    Atom, Display, False, FillTiled, GCFillStyle, GCTile, Pixmap, Success, Window, XGCValues,
    XWindowAttributes, Xlib, ZPixmap, XA_PIXMAP,
};
// use x11::xlib::{XGCValues, FillTiled, GCFillStyle, GCTile, False, XRootWindow, XDefaultScreen, XDefaultDepth, XCreatePixmap, XCreateGC, XFillRectangle, XFreeGC, XSync, XOpenDisplay};

pub const NULL: *const () = std::ptr::null();
pub const NULL_MUT: *mut () = std::ptr::null_mut();
use crate::state::State;
use glium::backend::glutin::glutin::platform::unix::RawContextExt;
use glium::backend::glutin::glutin::{ContextBuilder, WindowedContext};
use glium::backend::{Backend, Context};
use glium::Frame;
use std::fs::File;
use std::os::raw::c_void;
use std::ptr::{null, null_mut};
use std::rc::Rc;
use std::sync::{Arc, Mutex};
use std::time::{Duration, Instant};
use winit::dpi::Size;
use winit::platform::unix::x11::XConnection;
use winit::window::WindowBuilder;

lazy_static! {
    pub static ref X11_BACKEND: Mutex<Arc<XConnection>> =
        Mutex::new(XConnection::new(None).map(Arc::new).unwrap());
}

fn main() {
    let event_loop = glutin::event_loop::EventLoop::new();
    let wb = glutin::window::WindowBuilder::new();
    let cb = glutin::ContextBuilder::new();

    let xconn: Arc<XConnection> = Arc::clone(X11_BACKEND.lock().unwrap().deref());

    let xlib = &(Arc::clone(&xconn).xlib);

    let backend = sap_backend::SapBackend::new(&xconn, cb);

    let ctx = unsafe { Context::new(backend.clone(), true, Default::default()) };
    let ctx = ctx.unwrap();

    // let display = glium::Display::new(wb, cb, &event_loop).unwrap();

    // building the vertex buffer, which contains all the vertices that we will draw
    let vertex_buffer = {
        #[derive(Copy, Clone)]
        struct Vertex {
            position: [f32; 2],
        }

        implement_vertex!(Vertex, position);

        glium::VertexBuffer::new(
            &ctx,
            &[
                Vertex {
                    position: [-1.0, -1.0],
                },
                Vertex {
                    position: [-1.0, 1.0],
                },
                Vertex {
                    position: [1.0, -1.0],
                },
                Vertex {
                    position: [1.0, 1.0],
                },
                Vertex {
                    position: [1.0, -1.0],
                },
                Vertex {
                    position: [-1.0, 1.0],
                },
            ],
        )
        .unwrap()
    };

    // building the index buffer
    let index_buffer =
        glium::IndexBuffer::new(&ctx, PrimitiveType::TrianglesList, &[0u16, 1, 2, 3, 4, 5])
            .unwrap();

    let frag_buf = std::fs::read_to_string("./frag.glsl").unwrap();

    // compiling shaders and linking them together
    let program = program!(&ctx,
        140 => {
            vertex: "
                #version 140

                in vec2 position;

                void main() {
                    gl_Position = vec4(position, 0.0, 1.0);
                }
            ",

            fragment: frag_buf.as_str()
        },
    )
    .unwrap();

    let fps = 30.0;
    let spf = 1.0 / fps;
    let frame_duration = Duration::from_secs_f64(spf);

    let mut time = 0.0f32;
    let mut draw = move || {
        //let (xd, yd) = display.get_framebuffer_dimensions();

        time += spf as f32;

        let (xd, yd) = backend.get_framebuffer_dimensions();
        let dim = [xd as f32, yd as f32];

        let (mx, my) = backend.mouse_position();
        let (mx, my) = (mx, my);
        let mouse = [mx as f32, my as f32];

        // building the uniforms
        let uniforms = uniform! {
            resolution: dim,
            time: time,
            mouse: mouse,
        };

        // drawing a frame
        let mut target = Frame::new(Rc::clone(&ctx), (xd, yd)); //display.draw();
        target.clear_color(0.0, 0.0, 0.0, 0.0);
        target
            .draw(
                &vertex_buffer,
                &index_buffer,
                &program,
                &uniforms,
                &Default::default(),
            )
            .unwrap();
        target.finish().unwrap();
    };

    // Draw the triangle to the screen.
    draw();

    let mut state = State {
        time: 0.0,
        mouse_pos: (0, 0),
    };

    // the main loop
    event_loop.run(move |event, w, control_flow| {
        use glutin::event::*;
        use glutin::event_loop::ControlFlow;

        *control_flow = match event {
            Event::MainEventsCleared => {
                draw();
                let wait = Instant::now() + frame_duration;
                glutin::event_loop::ControlFlow::WaitUntil(wait)
            }
            Event::LoopDestroyed => ControlFlow::Exit,
            _ => ControlFlow::Poll,
        };
    });
}
